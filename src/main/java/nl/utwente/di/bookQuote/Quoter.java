package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    Map<String, Double> results;
    public Quoter() {
        results = new HashMap<>();
        results.put("1", 10.0);
        results.put("2", 45.0);
        results.put("3", 20.0);
        results.put("4", 35.0);
        results.put("5", 50.0);
    }

    double getBookPrice(String isbn) {
        return results.getOrDefault(isbn, 0.0);
    }
}